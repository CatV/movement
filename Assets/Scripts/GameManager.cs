﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Sphere")]
    [SerializeField] ObjectMove objectMove = default;
    [Header("UIElements")]
    [SerializeField] GameObject UI;
    [SerializeField] InputField timeInput = default;
    [SerializeField] InputField distanceOrRatius_Input = default;
    [SerializeField] InputField pikesOrLoops_Field = default;

    private void Start()
    {
        objectMove.OnMoveEnd += ()=> toggleUI(true);
    }

    public void CallObjectMove(int moveType)
    {
        float distanceOrRadius = 0;
        float time = 0;
        int pikesOrLoops = 0;

        if (float.TryParse(distanceOrRatius_Input.text, out distanceOrRadius)
            && float.TryParse(timeInput.text, out time)
            && int.TryParse(pikesOrLoops_Field.text, out pikesOrLoops))
        {
            objectMove.MoveObject((MoveType) moveType, distanceOrRadius, time, pikesOrLoops);
            toggleUI(false);
        }
        else
        {
            return;
        }
    }

    void toggleUI(bool toggle)
    {
        UI.SetActive(toggle);
    }
}
