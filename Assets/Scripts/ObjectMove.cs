﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove : MonoBehaviour
{
    public System.Action OnMoveEnd;

    public void MoveObject(MoveType moveType, float distance_or_radius, float time, int pikes_or_loops = 1)
    {
        switch (moveType)
        {
            case MoveType.Line:
                StartCoroutine(moveLine(distance_or_radius, time));
                break;
            case MoveType.Saw:
                StartCoroutine(moveSaw(distance_or_radius, time, pikes_or_loops));
                break;
            case MoveType.Spiral:
                StartCoroutine(moveSpiral(distance_or_radius, time, pikes_or_loops));
                break;
            default:
                break;
        }
    }

    IEnumerator moveLine(float distance, float time)
    {
        float speedPerSec = distance / time;
        float timePassed = 0;
        while (timePassed < time)
        {
            transform.Translate(speedPerSec * Time.deltaTime, 0, 0);
            timePassed += Time.deltaTime;
            yield return null;
        }
        transform.position = Vector3.zero;
        OnMoveEnd.Invoke();
    }

    IEnumerator moveSaw(float distance, float time, float pikes)
    {
        float pikeRate = 0.25f / (pikes / distance);
        float x = 0;
        float y = 0;
        float speedPerSec = distance / time;
        float timePassed = 0;
        while (timePassed < time)
        {
            x += speedPerSec * Time.deltaTime;
            y = (Mathf.Asin(Mathf.Sin((x / pikeRate - 1) / 2 * Mathf.PI)) / Mathf.PI + 0.5f) * 3;
            transform.position = new Vector3(x, y);
            timePassed += Time.deltaTime;
            yield return null;
        }
        transform.position = Vector3.zero;
        OnMoveEnd.Invoke();
    }

    IEnumerator moveSpiral(float radius, float time, float loops)
    {
        float rad = 0;
        float angle = 0;
        float x = 0;
        float y = 0;
        float speedPerSec = radius / time;
        float angleSpeeedPerSec = loops * 360 / time;
        float timePassed = 0;
        while (timePassed < time)
        {
            rad += speedPerSec * Time.deltaTime;
            angle += angleSpeeedPerSec * Time.deltaTime;
            x = rad * Mathf.Cos(angle * Mathf.Deg2Rad);
            y = rad * Mathf.Sin(angle * Mathf.Deg2Rad);
            transform.position = new Vector3(x, y);
            timePassed += Time.deltaTime;
            yield return null;
        }
        transform.position = Vector3.zero;
        OnMoveEnd.Invoke();
    }
}
